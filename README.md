# MRS-Design-Project

## What this is:

This is the Design Project of Saif Elkholy and Moetassem Abdelazmim. The work is supervised by Prof. Jamie Near. The main goal of this repo is to offer good version control for the python port of the work Prof. Near has.

The main python project can be found under `scripts/MRS-DP`


### System requirements:

- python3.6
- python3.6-tk (tkinter for GUI)
- python3-pip
- python-wxtools

### Installation:
- `sudo apt-get install python3.6 python3.6-tk python3-pip python-wxtools`
- sudo apt-get fsl, ants, c3_affine_tool linux packages. More info in the links below.
- `cd {RepositoryHome}/scripts/MRS-DP`
- `sudo pip3 install fslpy, antspy, nipype` //-r requirements.txt
- `./MRS-DP.py`

Links: 
1) Fsl:
2) ANTs:
3) c3d_affine_tool:
### System Overview:

![System Overview](https://bitbucket.org/selkholy/mrs-design-project/raw/b724b75077128c950fc8819ff11a5cc0e87f9242/Diagrams/mrs-design-python-project.jpg)

